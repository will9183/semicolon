import re

import traceback
from cogs.utils.utils import *
from cogs.utils.lang import *
import numpy as np
import datetime as dt
import random

top_cmd = 'top'


class GameScore:
    def __init__(self, score: typing.Union[int, float], success: bool = None, user: typing.Union[int, float] = None,
                 max_int: typing.Union[int, float] = None):
        self.score = score
        self.success = success
        self.max = max_int if max_int is not None else score  # what should be used for comparing maximums
        self.user = user


class Leaderboard(JsonManager):
    """
    Extend off this class with a _scoring(args) function and a game specified in super().__init__
    """
    def __init__(self, game: str):
        self._max_str = 'best'
        self._board_str = [self._max_str]
        self._game = game
        self._global_key = 'global_data'
        self._top_keys = {'user_data': 'ctx.author.id', self._global_key: "'global'"}  # use eval() on these
        self._colours = {0: discord.Colour.dark_blue(), 1: discord.Colour.dark_green(),
                         2: discord.Colour.green(), 3: discord.Colour.dark_purple()}
        super().__init__('leaderboard.json')

    def init_data(self, key: str, init_with=None):
        """Creates objects in json dict if non-existent"""
        super().init_data(key, init_with)
        if self._game not in self._data[key]:
            self._data[key][self._game] = {}
        for board_str in self._board_str:
            if board_str not in self._data[key][self._game]:
                self._data[key][self._game][board_str] = None

    def _save_score(self, snowflake: typing.Union[int, str], score: typing.Union[float, int]) -> GameScore:
        """Saves a score to json if it is a new best"""
        uid = str(snowflake)
        self.init_data(uid)
        old_max = self._data[uid][self._game][self._max_str]
        # grr this code repeats a little...
        new_record = True
        if old_max is not None:
            if old_max >= score:
                new_record = False
        if new_record:
            self._data[uid][self._game][self._max_str] = score
            self._save()
        return GameScore(old_max, success=None if old_max is None else new_record)

    def _scoring(self, ctx: commands.Context) -> GameScore:
        """Get the score based on user's input"""
        # This sample function sets an RNG seed to prevent any sort of manipulation based on other cogs
        # ... i don't actually know the random seeds would carry over from other cogs but i don't want to find out!
        random.seed(dt.datetime.now())
        return GameScore(0)

    def _process(self, ctx: commands.Context) -> dict:
        """Meta function for preparing the scoring dict"""
        score_obj = self._scoring(ctx)
        obj = {'score': score_obj}

        # basically, if it didn't fail, test if score/range was a new max
        if score_obj.success is None or score_obj.success:
            for key, item in self._top_keys.items():  # ...for user/guild/global
                obj[key] = self._save_score(eval(item), score_obj.max)
        return obj

    def _add_field(self, embed: discord.Embed, add_for: str, score_data: GameScore, record_type: str = None)\
            -> discord.Embed:
        if record_type is None:
            record_type = self._max_str
        if score_data.success:  # if user score was a new record compared to old score
            rtype = i18n[add_for]
            embed.add_field(name=i18n[record_type + '_name'].format(rtype),
                            value=i18n[f"{self._game}_value"].format(i18n[add_for + '_value'], rtype, record_type,
                                                                     score_data.score, plural(score_data.score)))
        return embed

    def _embed_process(self, key: str, item: GameScore, embed: discord.Embed) -> discord.Embed:
        """Adds fields for new bests to an embed"""
        return self._add_field(embed, key, item)

    def _description(self, item: GameScore, author: discord.Member) -> discord.Embed:
        """Returns description for an embed."""
        suffix = '_'+str(item.success) if item.success is not None else ''
        return i18n[self._game + '_description'+suffix].format(item, author)

    def _input_parser(self, ctx: commands.Context) -> typing.Union[None, discord.Embed]:
        """Returns a string if input is invalid, otherwise returns None. Should be overridden."""
        return None

    def process(self, ctx: commands.Context) -> discord.Embed:
        """Generates a score and returns the output."""
        errors = self._input_parser(ctx)
        if errors is not None:
            return errors

        embed = embed_author_template(ctx)
        obj = self._process(ctx)
        score = obj['score']
        del obj['score']  # prevent iterating on the score later
        embed.description = self._description(score, ctx.author)

        not_failure = score.success is None or score.success
        if not_failure:
            for key, item in obj.items():
                embed = self._embed_process(key, item, embed)

        colour_index = int(not_failure)+len(embed.fields)
        colour = self._colours[colour_index]
        if colour is not None:
            embed.colour = colour
        return embed

    def top(self, ctx: commands.Context, sort_flags: dict = None) -> discord.Embed:
        default_flags = {self._max_str: {'reverse': True}}
        if sort_flags is not None:
            for flag_key, flags in sort_flags:
                default_flags[flag_key] = flags

        errors = self._input_parser(ctx)
        if errors is not None:
            return errors

        # output string
        out_msg = i18n['top_msg']
        suffix = '_top_suffix'
        add_key = self._game
        if add_key+suffix not in i18n:
            add_key = 'generic'
        out_msg += i18n[add_key+suffix]

        embed = embed_author_template(ctx, subtle_author=True)
        embed.title = ctx.prefix+ctx.command.parent.name+' '+ctx.command.name
        global_key = eval(self._top_keys[self._global_key])
        records = {x: y[self._game] for x, y in self._data.items() if x != global_key and self._game in y}
        vals = list(records.values())
        if not vals:
            embed.description = i18n['no_leaderboard']
            return embed
        for key in vals[0]:
            key_records = [{'user_id': x, 'score': y[key]} for x, y in records.items() if y[key] is not None]
            flags = default_flags[key] if key in default_flags else {}
            sorted_records = sorted(key_records, key=lambda x: x['score'], **flags)

            placement, prev_record, formatted_records, author_added, ellipses = None, None, [], False, False
            for i, record in enumerate(sorted_records, 1):
                if record['score'] != prev_record:
                    placement = i
                is_author = str(ctx.author.id) == record['user_id']
                # we want to add a '...' to show the gap between author and top 10 if applicable (ie not 11th place)
                if author_added and placement > 10:
                    break
                elif is_author:
                    author_added = True
                elif placement > 10 and not author_added:
                    if not ellipses:
                        formatted_records.append('...')
                        ellipses = True
                    continue
                record['count'] = placement
                member = ctx.bot.get_user(int(record['user_id']))
                record['member'] = member if member is not None else record['user_id']
                formatted_records.append(out_msg.format(record).strip())
                # TODO: "and [x] more..."
            embed.add_field(name=i18n[key+'_top_name'], value='\n'.join(formatted_records))
        return embed

    def import_data(self, old_data_path: str):
        for filename in [f for f in os.listdir(old_data_path)]:
            filename_key = filename.replace('.txt', '') if re.match(r"\d{17,}\.txt", filename) else 'global'
            self.init_data(filename_key)
            uservalue = self._data[filename_key][self._game][self._max_str]
            with open(os.path.join(old_data_path, filename), 'r') as f:
                record = f.readlines()[int(filename_key == 'global')].strip()
                try:
                    record = int(record)
                except ValueError:
                    record = float(record)
                if uservalue is not None:
                    if not(record > uservalue):
                        continue
                self._data[filename_key][self._game][self._max_str] = record
        self._save()


class Lowderboard(Leaderboard):
    """Base class for Leaderboards that also track lowest scores"""
    def __init__(self, game):
        super().__init__(game)
        self._low_str = 'low'
        self._board_str = [self._max_str, self._low_str]

    def _save_score(self, snowflake: typing.Union[int, str], score: typing.Union[float, int]) -> tuple:
        gs_max = super()._save_score(snowflake, score)
        uid = str(snowflake)
        self.init_data(uid)
        old_min = self._data[uid][self._game][self._low_str]
        # grr this code repeats a little...
        new_min = score
        new_record = True
        if old_min is not None:
            if old_min <= score:
                new_min = old_min
                new_record = False
        self._data[uid][self._game][self._low_str] = new_min
        self._save()
        return gs_max, GameScore(old_min, success=None if old_min is None else new_record)

    def _embed_process(self, key: str, item: tuple, embed: discord.Embed) -> discord.Embed:
        old_max, old_min = item
        for old, record_for in [(old_max, self._max_str), (old_min, self._low_str)]:
            embed = self._add_field(embed=embed, add_for=key, score_data=old, record_type=record_for)
        return embed


class PointsLeaderboard(Lowderboard):
    def __init__(self):
        super().__init__('points')

    def _scoring(self, ctx: commands.Context) -> GameScore:
        super()._scoring(ctx)
        return GameScore(np.random.pareto(1))


class GuessLeaderboard(Leaderboard):
    def __init__(self):
        super().__init__('guess')
        self._length_limit = 100

    def _scoring(self, ctx: commands.Context) -> GameScore:
        super()._scoring(ctx)
        guess, max_guess = unpack_args(ctx)
        toguess = random.randint(1, max_guess)
        return GameScore(toguess, success=toguess == guess, max_int=max_guess, user=guess)

    def _input_parser(self, ctx: commands.Context) -> typing.Union[None, discord.Embed]:
        error = super()._input_parser(ctx)
        exargs = unpack_args(ctx)
        if error is None and ctx.command.name != top_cmd:
            guess, max_guess = unpack_args(ctx)
            if max_guess <= 1:
                error = 'max_guess_too_low'
            elif guess < 1:
                error = 'guess_too_low'
            elif guess > max_guess:
                error = 'guess_too_large'
            elif max(map(len, map(str, [guess, max_guess]))) > self._length_limit:
                error = 'number_too_long'
                exargs += (self._length_limit,)
        if error is not None:
            return error_gen(ctx, (i18n['error'] + i18n[self._game+'_error_'+error]).format(*exargs))
        return None


class FloatGuessLeaderboard(Leaderboard):
    def __init__(self):
        super().__init__('float_guess')
        self._length_limit = 22

    def _scoring(self, ctx: commands.Context) -> GameScore:
        super()._scoring(ctx)
        guess = unpack_args(ctx)[0]
        toguess = random.random()
        return GameScore(toguess, success=toguess == guess, max_int=1, user=guess)

    def _input_parser(self, ctx: commands.Context) -> typing.Union[None, discord.Embed]:
        error = super()._input_parser(ctx)
        exargs = unpack_args(ctx)
        if error is None and ctx.command.name != top_cmd:
            guess = unpack_args(ctx)[0]
            if not (0 <= guess < 1):
                error = 'range'
            elif max(map(len, map(str, [guess]))) > self._length_limit:
                error = 'number_too_long'
                exargs += (self._length_limit-2,)
        if error is not None:
            return error_gen(ctx, (i18n['error'] + i18n[self._game+'_error_'+error]).format(*exargs))
        return None


point_leaderboard = PointsLeaderboard()
guess_leaderboard = GuessLeaderboard()
float_leaderboard = FloatGuessLeaderboard()


class LeaderboardGames(commands.Cog, name='Leaderboard Games'):
    def __init__(self, bot):
        self.bot = bot

    @commands.group(invoke_without_command=True, aliases=['battle'])
    @commands.cooldown(rate=2, per=1, type=commands.BucketType.guild)
    async def points(self, ctx: commands.Context):
        """Generates a random score."""
        await ctx.send(embed=point_leaderboard.process(ctx))

    @points.command(name=top_cmd)
    async def points_top(self, ctx: commands.Context):
        """Lists the highest scores of ;points"""
        await ctx.send(embed=point_leaderboard.top(ctx))

    @points.command(name='import')
    @commands.is_owner()
    async def points_import(self, ctx: commands.Context):
        """Imports old data"""
        point_leaderboard.import_data('scores')
        await ctx.send(i18n['imported'])

    @commands.group(invoke_without_command=True, name='guess', aliases=['number'])
    @commands.cooldown(rate=2, per=1, type=commands.BucketType.guild)
    async def guess_cmd(self, ctx: commands.Context, guess: int, max_guess: int = 10):
        """
        Can you guess what number I'm thinking of?
        Optionally specify max_guess to change the range of numbers to [1, max_guess]
        """
        await ctx.send(embed=guess_leaderboard.process(ctx))

    @guess_cmd.command(name=top_cmd)
    async def guess_top(self, ctx: commands.Context):
        """Lists the highest scores of ;guess"""
        await ctx.send(embed=guess_leaderboard.top(ctx))

    @guess_cmd.command(name='import')
    @commands.is_owner()
    async def guess_import(self, ctx: commands.Context):
        """Imports old data"""
        guess_leaderboard.import_data('numbers')
        await ctx.send(i18n['imported'])

    @commands.group(invoke_without_command=True, name='float', aliases=['float_guess', 'fguess', 'floatg', 'fg'])
    @commands.cooldown(rate=2, per=1, type=commands.BucketType.guild)
    async def float_guess(self, ctx: commands.Context, guess: float):
        """
         Can you guess what decimal I'm thinking of? (Hint: Probably not.)
         """
        await ctx.send(embed=float_leaderboard.process(ctx))

    @float_guess.command(name=top_cmd)
    async def float_top(self, ctx: commands.Context):
        """Lists the highest scores of ;points"""
        try:
            await ctx.send(embed=float_leaderboard.top(ctx))
        except:
            traceback.print_exc()


def setup(bot):
    bot.add_cog(LeaderboardGames(bot))
