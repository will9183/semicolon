import discord
import logging
import datetime

import typing
from discord.ext import commands
from cogs.utils.lang import *
from cogs.utils.utils import *


class Deletable:
    # own class for message delete events bc raw bulk delete doesn't work how i expected and im too lazy to recode
    __slots__ = {'channel_id', 'message_id'}

    def __init__(self, channel_id, message_id):
        self.channel_id = channel_id
        self.message_id = message_id


async def joinlog_process(member: discord.Member, guild: discord.Guild, event: str):
    """
    Processes member joins and leaves
    :param member: discord.py member object
    :param guild: the originating guild
    :param event: the event that triggered this function. can be "JOIN", "LEAVE", "BAN", "UNBAN"
    """
    # TODO: localisation files??
    logchan = discord.utils.get(guild.text_channels, name=CONSTANTS['join_channel'])
    if logchan is None:
        return

    guildperms = guild.me.guild_permissions
    chanperms = guild.me.permissions_in(logchan)
    if not (chanperms.send_messages and chanperms.view_channel):
        return

    emojiindex = int(not chanperms.use_external_emojis)
    emoji = CONSTANTS['emojis'][event][emojiindex]
    new_join = ""
    reason = ""

    i18n_p = 'joinlog_'
    output = i18n[i18n_p+'prefix'] + i18n[i18n_p+event].strip()

    if event == "JOIN":
        if member.created_at >= (datetime.datetime.now() - datetime.timedelta(days=1)):
            new_join = CONSTANTS['emojis']['NEW_JOIN']
    elif event == "LEAVE":
        try:
            if guildperms.ban_members:
                await guild.fetch_ban(member)
                logging.info('{0!s} is banned, skipping {1} log'.format(member, event))
                return
        except discord.NotFound:
            pass
    elif event == "BAN":
        if guildperms.ban_members:
            ban = await guild.fetch_ban(member)
            reason = i18n[i18n_p+'reason'].format(ban.reason)

    foutput = output.format(emoji, member, guild.member_count, new_join, reason)
    await logchan.send(foutput, allowed_mentions=ping_user(member))


class Joinlog(commands.Cog):
    def __init__(self, bot: commands.Bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_member_join(self, member: discord.Member):
        await joinlog_process(member, member.guild, "JOIN")

    @commands.Cog.listener()
    async def on_member_remove(self, member: discord.Member):
        await joinlog_process(member, member.guild, "LEAVE")

    @commands.Cog.listener()
    async def on_member_ban(self, guild: discord.Guild, member: discord.Member):
        await joinlog_process(member, guild, "BAN")

    @commands.Cog.listener()
    async def on_member_unban(self, guild: discord.Guild, member: discord.Member):
        await joinlog_process(member, guild, "UNBAN")

    @commands.Cog.listener()
    async def on_user_update(self, before: discord.User, after: discord.User):
        # todo: check if "if after in x.members" works
        name_change = before.name != after.name
        disc_change = before.discriminator != after.discriminator
        if not (name_change or disc_change):
            return
        guilds = [x for x in self.bot.guilds if discord.utils.get(x.members, id=after.id)]
        for g in guilds:
            logchan = discord.utils.get(g.text_channels, name=CONSTANTS['join_channel'])
            if logchan is None:
                continue
            guildperms = g.me.guild_permissions
            chanperms = g.me.permissions_in(logchan)
            if not (chanperms.send_messages and chanperms.view_channel):
                continue

            i18n_p = 'joinlog_'
            output = []
            if name_change:
                output.append(i18n[i18n_p+'NAME'])
            if disc_change:
                if not name_change:
                    i18n_p += 'ONLY_'
                output.append(i18n[i18n_p+'DISCRIM'])
            await logchan.send('\n'.join(output).format(before, after))

    @commands.Cog.listener()
    async def on_message_edit(self, before: discord.Message, after: discord.Message):
        if not after.type == discord.MessageType.default:
            return
        if after.content == before.content:
            return
        log_chan = discord.utils.get(after.guild.text_channels, name=CONSTANTS['message_channel'])
        if log_chan is None:
            return
        perms = log_chan.permissions_for(log_chan.guild.me)
        if not (perms.read_messages and perms.send_messages):
            return
        if not before.content:
            before.content = i18n['message_blank_content']
        if not after.content:
            after.content = i18n['message_blank_content']

        message_type = 'edit'
        startkey = 'message_{}_'.format(message_type)
        mlang = {x.split(startkey)[1]: y for x, y in i18n.items() if x.startswith(startkey)}

        embed = discord.Embed(title=mlang['title'].format(after.channel),
                              timestamp=after.edited_at,
                              color=CONSTANTS['colors'][startkey[:-1]])
        embed.set_author(name=after.author.name, icon_url=after.author.avatar_url_as(format='png', size=128))
        embed.description = mlang['desc'].format(after.jump_url)
        embed.add_field(name=mlang['old_title'], value=shorten(before.content, 1024), inline=False)
        embed.add_field(name=mlang['new_title'], value=shorten(after.content, 1024), inline=False)
        await log_chan.send(embed=embed)

    async def message_delete(self, message: typing.Union[discord.Message, Deletable, list]):
        # get message data
        is_message = isinstance(message, discord.Message)
        if not is_message:
            channel = self.bot.get_channel(message.channel_id)
            guild = channel.guild
            message_id = message.message_id
        else:
            guild = message.guild
            channel = message.channel
            message_id = message.id

        if not guild:  # ignore DMs or somethin
            return

        # get channel
        log_chan = discord.utils.get(guild.text_channels, name=CONSTANTS['message_channel'])
        if log_chan is None:
            return
        # ensure perms
        perms = log_chan.permissions_for(log_chan.guild.me)
        if not (perms.read_messages and perms.send_messages):
            return

        # language variables
        message_type = 'delete'
        startkey = 'message_{}_'.format(message_type)
        mlang = {x.split(startkey)[1]: y for x, y in i18n.items() if x.startswith(startkey)}

        # embed formatting
        title = mlang['title'] if is_message else mlang['uncached_title']
        embed = discord.Embed(title=title.format(channel),
                              timestamp=discord.utils.snowflake_time(message_id),
                              color=CONSTANTS['colors'][startkey[:-1]])
        if isinstance(message, discord.Message):
            embed.set_author(name=message.author.name, icon_url=message.author.avatar_url_as(format='png', size=128))
            embed.description = message.system_content  # no shortening necessary, desc limit is at least 2000

        # TODO: add channel id and maybe emojis. also add to edit logs
        embed.set_footer(text=mlang['footer'].format(message_id))
        await log_chan.send(embed=embed)

    @commands.Cog.listener()
    async def on_raw_message_delete(self, payload: discord.RawMessageDeleteEvent):
        if payload.cached_message:
            send = payload.cached_message
        else:
            send = Deletable(channel_id=payload.channel_id, message_id=payload.message_id)
        await self.message_delete(send)

    @commands.Cog.listener()
    async def on_raw_bulk_message_delete(self, payload: discord.RawBulkMessageDeleteEvent):
        for mid in payload.message_ids:
            cached_msg = discord.utils.get(payload.cached_messages, id=mid)
            if cached_msg is None:
                cached_msg = Deletable(channel_id=payload.channel_id, message_id=mid)
            await self.message_delete(cached_msg)


def setup(bot):
    bot.add_cog(Joinlog(bot))
