from numpy import math
import re
from cogs.utils.utils import *
from cogs.utils.lang import *


async def is_lexi_lounge(ctx):
    return ctx.guild and ctx.guild.id == 179017865779871744


class SequenceNotFound(Exception):
    pass


async def msg_search(channel: discord.TextChannel, regex, limit=1):
    results = []
    iterations = 0
    async for msg in channel.history():
        iterations += 1
        rematch = regex.match(msg.content)
        if rematch:
            if rematch.groups():
                results.append(rematch.groups())
            else:
                results.append(rematch.group(0))
            if len(results) >= limit:
                if limit == 1:
                    results = results[0]
                return results, iterations
    return None, None


async def polls(channel: discord.TextChannel):
    return 'Whatever your heart needs to know!'


async def op(channel: discord.TextChannel):
    return 'op'


async def eyes(channel: discord.TextChannel):
    return '\N{EYES}'


async def counting(channel: discord.TextChannel):
    next_var, iterations = await msg_search(channel, re.compile(r'\d+'))
    if next_var:
        return int(next_var) + iterations
    raise SequenceNotFound('Next item not found')


async def fibonacci(channel: discord.TextChannel):
    next_vars, iterations = await msg_search(channel, re.compile(r'\d+'), limit=2)
    if next_vars:
        ints = list(map(int, next_vars))[::-1]
        sums = sum(ints)
        while iterations > 2:
            ints[0] = ints[1]
            ints[1] = sums
            sums = sum(ints)
            iterations -= 1
        return sums
    raise SequenceNotFound('Next item not found')


async def unicode(channel: discord.TextChannel):
    next_var, iterations = await msg_search(channel, re.compile(r'(.) \(U\+[\dABCDEF]{4}\)'), limit=1)
    if next_var:
        ordinance = ord(next_var[0])+iterations
        return '{0} (U+{1:0>4})'.format(chr(ordinance), hex(ordinance)[2:].upper())
    raise SequenceNotFound('Next item not found')


async def powers_of_two(channel: discord.TextChannel):
    next_var, iterations = await msg_search(channel, re.compile(r'\d+'), limit=1)
    if next_var:
        next_var = int(next_var)*int(math.pow(2, iterations))
        return '{} (2^{})'.format(next_var, int(math.log2(next_var)))
    raise SequenceNotFound('Next item not found')


async def xkcd_counting(channel: discord.TextChannel):
    next_var, iterations = await msg_search(channel, re.compile(r'https?://(?:www\.)?xkcd\.com/(\d+)'), limit=1)
    if next_var:
        next_var = int(next_var[0]) + iterations
        return '<https://xkcd.com/{}/>'.format(next_var)
    raise SequenceNotFound('Next item not found')


funcs = [polls, op, eyes, counting, fibonacci, unicode, powers_of_two, xkcd_counting]


class Lounge(commands.Cog):
    @commands.command(hidden=True)
    @commands.check(is_lexi_lounge)
    async def next(self, ctx, channel: discord.TextChannel):
        """[LCL] Posts the next expected message of a fun&games channel"""
        func = [x for x in funcs if x.__name__ == channel.name]
        if func:
            func = func[0]
            try:
                next_var = await func(channel)
            except SequenceNotFound:
                next_var = i18n['error']+i18n['lcl_not_found']
        else:
            next_var = i18n['lcl_channel_missing']
        await ctx.send(next_var)



def setup(bot):
    bot.add_cog(Lounge(bot))
