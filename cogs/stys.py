from cogs.utils.utils import *

STREAM_PREFIX = "stream-"
ADD_MSG = {True: 'added', False: 'removed'}
DELETE_DELAY = 5.0
STYS_ID = 144680570708951040


async def is_stys(ctx):
    return ctx.guild and ctx.guild.id == STYS_ID


class STYS(commands.Cog):
    @commands.Cog.listener()
    async def on_member_join(self, member: discord.Member):
        if member.guild.id == STYS_ID:
            await member.add_roles(member.guild.get_role(210252019863126017))

    @commands.group(aliases=['stys'], invoke_without_command=True)
    @commands.bot_has_guild_permissions(manage_roles=True)
    @commands.check(is_stys)
    async def stream(self, ctx, user: str, action: typing.Optional[bool] = None):
        """
        Toggles on/off a stream notification role. Optionally accepts bool-like statement (on/off/true/false/1/0/etc.)
        """
        async with ctx.typing():
            role = None
            search_for = (STREAM_PREFIX+user).lower()
            for r in ctx.guild.roles:
                if r.name.lower() == search_for:
                    role = r
                    break
            if role is None:  # if role was found
                await ctx.send(f"{ctx.author.mention}: I couldn't find that role. Try `{ctx.prefix}{ctx.command} list`",
                               delete_after=DELETE_DELAY)
                return
            if role >= ctx.guild.me.top_role:
                await ctx.send("That role is inaccessible, it should be below my highest role. mods pls fix")
                return
            if action is None:
                action = False if role in ctx.author.roles else True
            func = ctx.author.add_roles if action else ctx.author.remove_roles
            await func(role, reason=f'Requested via {ctx.prefix}{ctx.command}')
            await ctx.send(f"{ctx.author.mention}: Successfully {ADD_MSG[action]} the role {r.name.lower()}.", delete_after=DELETE_DELAY, allowed_mentions=ping_user(ctx))
            await ctx.message.delete(delay=DELETE_DELAY, reason=f'{ctx.prefix}{ctx.command} cleanup')

    @stream.command(name='list')
    async def stream_list(self, ctx):
        """
        Lists the valid stream roles.
        """
        async with ctx.typing():
            rnames = []
            for r in ctx.guild.roles:
                if r.name.lower().startswith(STREAM_PREFIX):
                    rnames.append(r.name.lower()[len(STREAM_PREFIX):])
            await ctx.send('Valid stream roles: '+comma_separator(rnames))


def setup(bot):
    bot.add_cog(STYS(bot))
