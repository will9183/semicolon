import re
import inspect
from typing import List

from cogs.utils.utils import *
from cogs.utils.lang import *
import datetime
import humanize
import aiohttp
import random


class Miscellaneous(commands.Cog):
    @commands.command(name='eval')
    @commands.is_owner()
    async def eval_cmd(self, ctx, *, code: str):
        """Evaluates code"""
        # Code s̶t̶o̶l̶e̶n̶ used from HTSTEM's DiscordBot project, under the MIT license
        # https://gitlab.com/HTSTEM/DiscordBot/blob/python-ext.com/LICENSE
        env = {
            'ctx': ctx,
            'bot': ctx.bot,
            'guild': ctx.guild,
            'author': ctx.author,
            'message': ctx.message,
            'channel': ctx.channel
        }
        env.update(globals())

        try:
            result = eval(code, env)

            if inspect.isawaitable(result):
                result = await result

            colour = 0x00FF00
        except Exception as e:
            result = type(e).__name__ + ': ' + str(e)
            colour = 0xFF0000

        embed = discord.Embed(colour=colour, title=code, description='```py\n{}```'.format(result))
        embed.set_author(name=ctx.author.display_name, icon_url=ctx.author.avatar_url)
        await ctx.send(embed=embed)

    @commands.command(name='-;', aliases=['_;', 'w;', 'n;'], hidden=True)
    @commands.check(is_chip)
    async def cry(self, ctx: commands.Context):
        """Cries. It's funny because it triggers when you type ;-;"""
        if ctx.prefix == ';':
            await ctx.send(ctx.prefix + ctx.invoked_with)

    @commands.group(aliases=['error'], invoke_without_command=True)
    @commands.is_owner()
    async def crash(self, ctx: commands.Context):
        """Throws an exception"""
        raise Exception("Uh-oh oopsie-woopsie")

    @crash.command(aliases=['subcommand'])
    @commands.is_owner()
    async def sub(self, ctx: commands.Context, argument='arrrg'):
        """Throws an exception"""
        await ctx.send(argument)
        raise Exception("Uh-oh oopsie-woopsie")

    @commands.command(aliases=['pong', 'pog', 'pingpong'])
    @commands.cooldown(rate=1, per=3, type=commands.BucketType.channel)
    async def ping(self, ctx: commands.Context):
        """Is the bot alive? If you're reading this, probably."""
        UNIT = 'microseconds'  # can also be 'milliseconds', 'seconds'

        cmdstr = ctx.invoked_with
        cmdstr = cmdstr[0].upper() + cmdstr[1:] + '!'
        output = [cmdstr]
        msg = await ctx.send(output[0])

        delay = msg.created_at - ctx.message.created_at
        output.append('Delay: ' + humanize.naturaldelta(delay, minimum_unit=UNIT))
        ltncy = datetime.timedelta(seconds=ctx.bot.latency)
        output.append('WebSocket: ' + humanize.naturaldelta(ltncy, minimum_unit=UNIT))

        await msg.edit(content='\n'.join(output))

    @commands.command()
    @commands.is_owner()
    async def echo(self, ctx: commands.Context, *, text: str):
        await ctx.send(text)

    @commands.command()
    @commands.cooldown(rate=1, per=5, type=commands.BucketType.channel)
    async def reddit(self, ctx: commands.Context, subreddit: str, sort: str = "hot", time: str = "day"):
        """Grabs a recent image from the specified subreddit.
        'sort' can be one of 'hot', 'new', 'rising', 'top', 'controversial'
        'time' is used with 'top' and 'controversial' and can be one of 'hour', 'day', 'week', 'month', 'year', 'all'"""
        async with ctx.typing():
            sort = sort.lower()
            time = time.lower()
            # input validation
            if sort not in CONSTANTS['reddit_sort']:
                await ctx.send(i18n['invalid_argument'].format('sort', '`, `'.join(CONSTANTS['reddit_sort'])))
                return
            url = f'https://www.reddit.com/r/{subreddit}/{sort}/.json?count=100&limit=100'
            if sort in CONSTANTS['reddit_timeable']:
                if time not in CONSTANTS['reddit_time']:
                    await ctx.send(i18n['invalid_argument'].format('time', '`, `'.join(CONSTANTS['reddit_time'])))
                    return
                url += f'&t={time}'

            async with ctx.bot.session.get(url, headers={"User-Agent": "semicolon-bot:v1.1 (/u/noellekiq)"}) as r:
                if r.status == 200:
                    jsondata = await r.json()
                else:
                    error = error_gen(ctx, i18n['reddit_connection_failure'].format('reddit', r.status, await r.text()))
                    await ctx.send(embed=error)
                    return

            if 'error' not in jsondata:
                posts = jsondata['data']['children']
                if isinstance(ctx.channel, discord.TextChannel):
                    includensfw = ctx.channel.is_nsfw()
                else:
                    includensfw = True
                viableposts = []
                for post in posts:
                    pdata = post['data']
                    if "post_hint" in pdata:
                        if pdata['post_hint'] == 'image' and (not pdata['over_18'] or (pdata['over_18'] and includensfw)):
                            viableposts.append(pdata['url'])
                if viableposts:
                    await ctx.send(random.choice(viableposts))
                else:
                    await ctx.send('No viable posts found for this channel!')
            else:
                await ctx.send('Failed to get sub data! Are you sure that exists?')

    @commands.command(name='count')
    @commands.is_owner()
    async def cmd_count(self, ctx: commands.Context, user: typing.Union[discord.User, int], *, regex: str):
        """Counts messages containing regex from a user."""
        async with ctx.typing():
            if not isinstance(user, int):
                uid = user.id
            else:
                uid = user
            re_search = re.compile(regex, re.IGNORECASE)
            if isinstance(ctx.channel, discord.DMChannel):
                chans: List[discord.DMChannel] = [ctx.channel]
            else:
                def canview(channel: discord.TextChannel, member: discord.Member):
                    perms = channel.permissions_for(member)
                    return perms.read_messages and perms.read_message_history
                chans: List[discord.TextChannel] = [x for x in ctx.guild.text_channels if canview(x, ctx.guild.me)]
            count = 0
            for chan in chans:
                async for msg in chan.history(limit=None):
                    if msg.author.id != uid:
                        continue
                    for match in re_search.findall(msg.content):
                        count += 1
            await ctx.send(i18n['count_cmd'].format(count, user, regex, len(chans), plural(count), plural(len(chans))))

    @commands.command(aliases=['add', 'join'])
    @commands.cooldown(rate=1, per=5, type=commands.BucketType.channel)
    async def invite(self, ctx):
        """Add me to your server NOT!"""
        await ctx.send("Please DM Will9183 for a link!")


def setup(bot):
    bot.add_cog(Miscellaneous(bot))
