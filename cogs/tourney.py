import logging
import os

import discord
import typing
from discord.ext import commands
import json
import random
import traceback
import re
from cogs.utils.utils import *

# TODO/NOTES: cache the tourney file with a __init__ function or whatever it's called. add an owner only command to update.


class Tourney(commands.Cog):
    def __init__(self, bot: commands.Bot):
        self.bot = bot
        self.TOURNEY_USERDATA_FILE = data_path('tourney_users.json')
        self.TOURNEY_VOTEDATA_FILE = data_path('tourney_votes.json')
        self.TOURNEY_FILE = data_path('tourney.json')
        self.USERDATA_CACHE = {}
        self.TOURNEY_CACHE = {"KEY": None}
        self.VOTE_CACHE = {}

        # creates files if nonexistent
        for filename in [self.TOURNEY_USERDATA_FILE, self.TOURNEY_VOTEDATA_FILE]:
            if not os.path.isfile(filename):
                with open(filename, 'a') as f:
                    f.write("{}")

        with open(self.TOURNEY_FILE, 'r') as f:
            file = json.load(f)
            self.RAW_TOURNEY_CACHE = file
            self.TOURNEY_CACHE["KEY"] = 'S{}R{}'.format(file['season'], file['round'])
            self.TOURNEY_CACHE["TITLE"] = "{}, Round {}".format(file['title'], file['round'])
            self.TOURNEY_CACHE["BRACKET"] = file['bracket']
        shortkey = self.TOURNEY_CACHE['KEY']

        with open(self.TOURNEY_USERDATA_FILE) as j:
            self.RAW_USERDATA_CACHE = json.load(j)
        if shortkey not in self.RAW_USERDATA_CACHE:
            self.RAW_USERDATA_CACHE[shortkey] = {}
        self.USERDATA_CACHE = self.RAW_USERDATA_CACHE[shortkey]

        with open(self.TOURNEY_VOTEDATA_FILE) as j:
            self.RAW_VOTE_CACHE = json.load(j)
        if shortkey not in self.RAW_VOTE_CACHE:
            self.RAW_VOTE_CACHE[shortkey] = {}
        self.VOTE_CACHE = self.RAW_VOTE_CACHE[shortkey]

        for emoji in self.TOURNEY_CACHE["BRACKET"]:
            dictkey = emoji.split(' ')[0]
            if dictkey not in self.VOTE_CACHE:
                self.VOTE_CACHE[dictkey] = []
        self.write_votedata()

    def write_userdata(self):
        with open(self.TOURNEY_USERDATA_FILE, 'w') as j:
            json.dump(self.RAW_USERDATA_CACHE, j)

    def write_votedata(self):
        with open(self.TOURNEY_VOTEDATA_FILE, 'w') as j:
            json.dump(self.RAW_VOTE_CACHE, j)

    def fill_userdata(self, user_id: typing.Union[str, int]):
        user_id = str(user_id)
        if user_id not in self.USERDATA_CACHE:
            self.USERDATA_CACHE[user_id] = {}
            self.USERDATA_CACHE[user_id]["SEEN_PROMPT"] = False
            self.USERDATA_CACHE[user_id]["SLIDES_VOTED"] = []
            self.write_userdata()

    def get_slide(self, user_id: typing.Union[str, int]):
        user_id = str(user_id)
        open_slides = []
        for x in range(0, len(self.TOURNEY_CACHE["BRACKET"]), 2):
            if x not in self.USERDATA_CACHE[user_id]["SLIDES_VOTED"]:
                open_slides.append(x)
        if len(open_slides) > 0:
            random.seed(int(user_id))
            return random.choice(open_slides)
        else:
            return None

    def prompt(self, user_id: typing.Union[str, int]):
        user_id = str(user_id)
        slide = self.get_slide(user_id)
        if slide is None:
            return 'Thank you, you have voted on everything! :)'
        options = [x.split(' ')[0] for x in self.TOURNEY_CACHE["BRACKET"][slide:slide + 2]]
        return f'{chr(127462)} {options[0]}        {chr(127463)} {options[1]}'

    def vote(self, user_id: typing.Union[str, int], option: str):
        user_id = str(user_id)
        slide = self.get_slide(user_id)
        if slide is not None:
            options = [x.split(' ')[0] for x in self.TOURNEY_CACHE["BRACKET"][slide:slide + 2]]
            votes = [1 if option == 'a' else 0, 1 if option == 'b' else 0]  # there's gotta be a better way to do this
            for x in range(0, 2):
                self.VOTE_CACHE[options[x]].append(votes[x])
            self.write_votedata()
            self.USERDATA_CACHE[user_id]["SLIDES_VOTED"].append(slide)
            self.write_userdata()
        return self.prompt(user_id)

    @commands.group(invoke_without_command=True, case_insensitive=True, name='vote', aliases=['v', 'emoji'])
    @commands.dm_only()
    async def votecmd(self, ctx: commands.Context, option: str = None):
        """Vote on the Emoji Tournament!"""
        try:
            user_id = str(ctx.author.id)
            self.fill_userdata(user_id)

            if option is None:
                if not self.USERDATA_CACHE[user_id]["SEEN_PROMPT"]:
                    await ctx.send(f"You are beginning voting for {self.TOURNEY_CACHE['TITLE']}! Here you pick between two emojis and select the one you like more. Simply type `;v a` or `;v b` to vote. To get your current two emojis again, type `;v`.")
                    self.USERDATA_CACHE[user_id]["SEEN_PROMPT"] = True
                    self.write_userdata()
                await ctx.send(self.prompt(user_id))
            elif option.lower() in ["a", "b"]:
                if not self.USERDATA_CACHE[user_id]["SEEN_PROMPT"]:
                    await ctx.send("You don't have a prompt yet! Type `;vote` first to get a prompt.")
                    return
                await ctx.send(self.vote(user_id, option.lower()))
            else:
                await ctx.send("Invalid input!")
        except Exception as e:
            traceback.print_exc()

    @votecmd.command(name='reset')
    @commands.dm_only()
    @commands.is_owner()
    async def reset(self, ctx: commands.Context):
        """Refreshes the database files"""
        self.__init__(self.bot)
        await ctx.send("Caches reset")

    @votecmd.command(name='tally')
    @commands.is_owner()
    async def tally(self, ctx: commands.Context, commit: bool = False):
        """Tallies the results"""
        #print(self.VOTE_CACHE)
        emojisss = {}
        for emoji, votes in self.VOTE_CACHE.items():
            #print(emoji, votes)
            emojisss[emoji] = {}
            edict = emojisss[emoji]
            edict['percent'] = round((sum(votes)/len(votes)) * 100, 2)
            edict['votes'] = sum(votes)
            edict['total'] = len(votes)
        emoji2 = {k: v for k, v in sorted(emojisss.items(), key=lambda item: item[1]['percent'], reverse=True)}
        eformat = [f"{y['percent']}% ({y['votes']}/{y['total']} votes): {x}" for x, y in emoji2.items()]
        surviving = eformat[:len(eformat)//2]
        eliminated = eformat[len(eformat)//2:]
        # this is stupid i should pull directly from emoji2 but lazy
        elim_emo = [x.split(' ')[::-1][0] for x in eliminated]

        outlist = surviving + ['Eliminated:'] + eliminated
        if commit:
            outlist.append('Executing emojis... please restart the bot to ensure stability.')
        output = '\n'.join(outlist)
        await ctx.send(output)

        if not commit:  # if this is a "trial run" (checking results)
            return

        # eliminate emojis
        newbracket = self.RAW_TOURNEY_CACHE['bracket'].copy()
        for emo in newbracket:
            if emo.split(' ')[0] in elim_emo:
                self.RAW_TOURNEY_CACHE['bracket'].remove(emo)
        self.RAW_TOURNEY_CACHE['round'] += 1
        with open(self.TOURNEY_FILE, 'w') as f:
            json.dump(self.RAW_TOURNEY_CACHE, f)
        self.__init__(self.bot)  # TODO: data doesn't get properly loaded.

    @votecmd.command(name='retally')
    @commands.dm_only()
    @commands.is_owner()
    async def vote_retally(self, ctx: commands.Context, after_id: int):
        async with ctx.typing():
            for emojikey in self.VOTE_CACHE:
                self.VOTE_CACHE[emojikey] = []
            for user_id in self.USERDATA_CACHE:
                user_id = int(user_id)
                user = self.bot.get_user(user_id)
                emojis = None
                # aid = 714298405953863740 if user_id in [197876594755371008, 188404112910581760, 240995021208289280, 82629898316808192] else after_id
                # aid = 714288438001532968 if user_id in [185229437787176962, 185229437787176962] else aid
                async for x in user.history(oldest_first=True, after=after_id):
                    content = x.content.lower()
                    rematch = re.match(';v(ote)? ([ab])', content)
                    if emojis is not None and x.author.id != self.bot.user.id and rematch:
                        vote = rematch.groups()[::-1][0]
                        votes = [1 if vote == 'a' else 0, 1 if vote == 'b' else 0]
                        for z in range(0, 2):
                            logging.info(f'{user_id}: Saving {votes[z]} to {emojis[z]}')
                            self.VOTE_CACHE[emojis[z]].append(votes[z])
                        emojis = None
                        continue
                    rematch = re.match(chr(127462) + ' (.+) {8}' + chr(127463) + ' (.+)', content)
                    if x.author.id == self.bot.user.id and rematch:
                        emojis = [rematch.group(1), rematch.group(2)]

            self.write_votedata()
            await ctx.send('Vote recovery complete!')


def setup(bot):
    bot.add_cog(Tourney(bot))
