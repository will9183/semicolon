from cogs.utils.utils import *
import asyncio

PRONOUN_PREFIX = CONSTANTS['pronoun_prefix']
PRONOUN_WHITELIST = CONSTANTS['pronoun_whitelist']
LOCK_DATABASE = asyncio.Lock()
LOCK_LOCKS = asyncio.Lock()
plockjson = data_path('pronoun_locks.json')
pjson = data_path('pronouns.json')


async def p_locks(user_id, guild_id=None):
    """Gets the current locked server IDs, or sets a provided server as locked"""
    user_id = str(user_id)
    with open(plockjson) as j:
        data = json.load(j)
    if guild_id is None:
        if user_id in data:
            return data[user_id]
        else:
            return []
    else:
        async with LOCK_LOCKS:
            if user_id in data:
                if guild_id in data[user_id]:
                    data[user_id].remove(guild_id)
                    if not data[user_id]:  # if list is empty
                        del data[user_id]
                    added = False
                else:
                    data[user_id].append(guild_id)
                    added = True
            else:
                data[user_id] = [guild_id]
                added = True
            with open(plockjson, 'w') as j:
                json.dump(data, j)

            return added


async def p_write(user_id, pronouns, add: bool = True, overwrite: bool = False):
    """
    Writes a pronoun to the database
    :param overwrite: whether to replace all pronouns with the new list
    :param user_id: the user's id
    :param pronouns: the pronoun(s) to add
    :param add: whether to add (True) or remove (False)
    """
    async with LOCK_DATABASE:
        if isinstance(pronouns, str):
            pronouns = [pronouns]
        user_id = str(user_id)
        with open(pjson) as j:
            data = json.load(j)
        if user_id not in data:
            data[user_id] = []
        if not overwrite:
            for pronoun in pronouns:
                if add:
                    if pronoun not in data[user_id]:
                        data[user_id].append(pronoun)
                else:
                    if pronoun in data[user_id]:  # it may not always be there
                        data[user_id].remove(pronoun)
        else:
            data[user_id] = pronouns
        if not data[user_id]:  # if list is empty now
            del data[user_id]  # (saves storage space :p)
        with open(pjson, 'w') as j:
            json.dump(data, j)


def p_read(user_id):
    """
    Reads pronouns from the database
    :param user_id: user id
    :return: list of pronouns
    """
    user_id = str(user_id)
    with open(pjson) as j:
        data = json.load(j)
    if user_id in data:
        return data[user_id]
    else:
        return []


class Pronouns(commands.Cog):
    def __init__(self):
        # creates files if nonexistent
        for filename in [plockjson, pjson]:
            if not os.path.isfile(filename):
                with open(filename, 'a') as f:
                    f.write("{}")

    async def p_memberupdate(self, guild, members=None, overwrite: bool = False):
        """Updates the pronoun roles for a list of users.
        :param guild: The guild to update
        :param members: Optional list of members to update, uses member list of guild by default
        :param overwrite: Whether or not to remove pronoun roles from users not stored in the database
        """
        if members is None:
            members = guild.members
        roles = []
        for role in guild.roles:
            if role.name.lower().startswith(PRONOUN_PREFIX):
                roles.append(role)
        with open(pjson) as j:
            data = json.load(j)
        with open(plockjson) as lj:
            lockdata = json.load(lj)
        for member in members:
            uid = str(member.id)
            if uid in data:
                if uid in lockdata:
                    if guild.id in lockdata[uid]:
                        continue
                pronouns = data[uid]
                toadd = []
                for pronoun in pronouns:
                    for role in roles:
                        if role.name.lower() == PRONOUN_PREFIX + pronoun:
                            toadd.append(role)
                for role in toadd:
                    await member.add_roles(role)
                if overwrite:
                    for role in roles:
                        if role not in toadd:
                            await member.remove_roles(role)

    async def p_roleupdate(self, role):
        """When a role is created or updated, this function is called to add new pronoun roles to users
        :param role: The role to scan
        """
        # TODO: this is inefficient. find the role first
        # TODO: remove the role from users if it starts with PRONOUN_PREFIX
        rolename = role.name.lower()
        if not rolename.startswith(PRONOUN_PREFIX):
            return
        guild = role.guild
        if not (guild.me.guild_permissions.manage_roles and guild.me.top_role > role):
            return
        with open(pjson) as j:
            data = json.load(j)
        for userid, pronouns in data.items():
            member = guild.get_member(int(userid))
            if member is not None:
                for pronoun in pronouns:
                    if PRONOUN_PREFIX + pronoun == rolename:
                        await member.add_roles(role)

    @commands.Cog.listener()
    async def on_member_join(self, member):
        await self.p_memberupdate(member.guild, [member])

    @commands.Cog.listener()
    async def on_guild_join(self, guild):
        await self.p_memberupdate(guild)

    @commands.Cog.listener()
    async def on_guild_role_create(self, role):
        await self.p_roleupdate(role)

    @commands.Cog.listener()
    async def on_guild_role_update(self, before, after):
        await self.p_roleupdate(after)

    @commands.group(invoke_without_command=True, case_insensitive=True, aliases=['pronouns'], help=
                    "Sets a pronoun role across all participating servers.\n"+
                    "Usage examples: `{0}pronoun on he/him`, `{0}pronoun she`, `{0}pronoun disable any`\n".format(CONSTANTS['prefix_str'])+
                    "To setup, use `{}setup pronouns`.\n".format(CONSTANTS['prefix_str'])+
                    "Make sure semicolon has the permissions to manage roles, and its highest role is above every pronoun role!",
                    usage='[enable/disable] <pronoun>')
    @commands.cooldown(rate=1, per=10, type=commands.BucketType.user)
    async def pronoun(self, ctx, action: typing.Optional[bool] = None, *, pronoun: str):
        pronoun = pronoun.lower()
        if pronoun == 'pronouns' and not action and action is not None:
            pronoun = 'no pronouns'
            action = None
        for p_allowed in PRONOUN_WHITELIST:
            for p_split in p_allowed.split('/') + [p_allowed.split(' ')[0]]:
                if pronoun == p_split:
                    pronoun = p_allowed
        output = ""
        success = []
        failure = []
        POSITIVE_ACTION = "ADDED"
        NEGATIVE_ACTION = "REMOVED"
        POSITIVE_STATUS = "SUCCESS"
        NEGATIVE_STATUS = "FAILURE"
        POSITIVE_EMOJI = "<:greenTick:328630479886614529>"
        NEGATIVE_EMOJI = "<:redTick:328630479576104963>"
        CHECKLOCKS = True
        LOCKED_SERVERS = await p_locks(ctx.author.id)
        if len(LOCKED_SERVERS) > 0:
            SERVER_LOCKED = ctx.guild.id in LOCKED_SERVERS
        else:
            SERVER_LOCKED = False
            CHECKLOCKS = False
        if SERVER_LOCKED:
            output = "As you ran this command in a locked guild, only the current guild was modified."

        def p_output(status, name, message):
            if status == POSITIVE_STATUS:
                EMOJI = POSITIVE_EMOJI
                APPENDTO = success
            elif status == NEGATIVE_STATUS:
                EMOJI = NEGATIVE_EMOJI
                APPENDTO = failure
            APPENDTO.append(f"{EMOJI} **{name}**: {message}")

        guild_list = [ctx.guild] if SERVER_LOCKED else ctx.bot.guilds
        for guild in guild_list:
            if not guild.unavailable:
                pronounserver = False
                author = guild.get_member(ctx.author.id)
                p_roles = []
                if author is not None:
                    for role in guild.roles:
                        rolename = role.name.lower()
                        if rolename.startswith(PRONOUN_PREFIX):
                            pronounserver = True  # because i want to output if it's missing a role
                        if rolename == PRONOUN_PREFIX + pronoun:
                            p_roles.append(role)
                    if pronounserver:
                        botself = guild.me
                        botperms = botself.guild_permissions
                        bottop = botself.top_role

                        # Failure Checks
                        if CHECKLOCKS:
                            if guild.id in LOCKED_SERVERS and ctx.guild.id != guild.id:
                                p_output(NEGATIVE_STATUS, guild.name, "Skipped as you disabled syncing roles here")
                                continue
                        if not botperms.manage_roles:
                            p_output(NEGATIVE_STATUS, guild.name, "Lacking `MANAGE_ROLES` permissions.")
                            continue
                        if len(p_roles) == 0:
                            if pronoun in PRONOUN_WHITELIST:
                                createdrole = await guild.create_role(name=PRONOUN_PREFIX + pronoun)
                                p_roles.append(createdrole)
                            else:
                                p_output(NEGATIVE_STATUS, guild.name, f"Role not found and not in role whitelist. (`{ctx.prefix}pronoun whitelist`)")
                                continue

                        for p_role in p_roles:  # filters out roles the bot cannot manage
                            if not (bottop > p_role):
                                p_roles.remove(p_role)

                        if len(p_roles) == 0:
                            p_output(NEGATIVE_STATUS, guild.name, f"Cannot manage pronoun role, is higher or equal to the bot's highest role")
                            continue

                        while len(p_roles) > 1:  # deletes duplicates
                            firstrole = p_roles[0]
                            p_roles.remove(firstrole)
                            await firstrole.delete()

                        for p_role in p_roles:
                            if action is None:
                                if p_role not in author.roles:
                                    action = True
                                else:
                                    action = False
                            actiondesc = POSITIVE_ACTION if action else NEGATIVE_ACTION
                            if action:
                                await author.add_roles(p_role, reason="Pronoun Sync")
                            elif not action:
                                await author.remove_roles(p_role, reason="Pronoun Sync")
                            p_output(POSITIVE_STATUS, guild.name, f"Successfully {actiondesc} `{p_role.name}`.")

        footer = ""
        if action is not None and not SERVER_LOCKED:
            await p_write(ctx.author.id, pronoun, action)
            actiondesc = "added to" if action else "removed from"
            footer = f"Pronoun {actiondesc} database."
        outputlist = sorted(success, key=str.lower) + sorted(failure, key=str.lower)
        try:
            await private_message(ctx, '\n'.join([output, '\n'.join(outputlist), footer]))
        except discord.Forbidden:
            await ctx.send(i18n["pronoun_dm_fail"].format(ctx.author.mention, pronoun, actiondesc.split(' ')[0]),
                           delete_after=CONSTANTS['self_delete'])

    @pronoun.command(name='help', hidden=True)
    async def p_help(self, ctx):
        """Help! I need somebody-- Help!"""
        # TODO: post the actual help message
        await ctx.send(ctx.prefix+'help '+ctx.command.parent.name)

    @pronoun.command(name='database')
    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    async def p_database(self, ctx):
        """Sends you your current pronouns and locked servers"""
        pronouns = p_read(ctx.author.id)
        LOCKED_SERVERS = await p_locks(ctx.author.id)
        guilds = []
        for guildid in LOCKED_SERVERS:
            guild = ctx.bot.get_guild(guildid)
            if guild is not None:
                guilds.append(guild.name)
            else:
                guilds.append(guildid)
        guilds = sorted(guilds, key=str.lower)
        for lst in [pronouns, guilds]:
            if len(lst) == 0:
                lst.append('None')
        pronoun_output = f"Pronouns: `{'`, `'.join(pronouns)}`"
        lock_output = f"Locked Guilds: `{'`, `'.join(guilds)}`"
        output = ["**Your data:**", pronoun_output, lock_output]
        await private_message(ctx, '\n'.join(output))

    @pronoun.command(name='import', brief='Replaces your saved data with pronouns from the current server')
    @commands.cooldown(rate=1, per=60, type=commands.BucketType.user)
    async def p_import(self, ctx):
        """Imports your pronoun data from the current server into the database, replacing your existing entries.
        This will replace your pronouns on all unlocked servers.
        Runs regardless of whether your current server is locked or not."""

        pronouns = []
        for role in ctx.author.roles:
            rolename = role.name.lower()
            if rolename.startswith(PRONOUN_PREFIX):
                pronouns.append(rolename[len(PRONOUN_PREFIX):])
        await p_write(ctx.author.id, pronouns, overwrite=True)

        LOCKED_SERVERS = await p_locks(ctx.author.id)
        for guild in ctx.bot.guilds:
            if guild.id in LOCKED_SERVERS or guild.unavailable:
                continue
            author = guild.get_member(ctx.author.id)
            if author is None or not guild.me.guild_permissions.manage_roles:
                continue
            p_roles = {"add": [], "allroles": []}
            toprole = guild.me.top_role
            for role in guild.roles:
                if not (toprole > role):
                    continue
                rolename = role.name.lower()
                if rolename.startswith(PRONOUN_PREFIX):
                    key = "add" if rolename[len(PRONOUN_PREFIX):] in pronouns else "allroles"
                    if role not in p_roles[key]:
                        p_roles[key].append(role)
            for role in p_roles["add"]:
                await author.add_roles(role, reason="Pronoun Import")
            for role in p_roles["allroles"]:
                if role not in p_roles["add"]:
                    await author.remove_roles(role, reason="Pronoun Import")

        await ctx.send(
            f"Your pronouns have been updated across eligible servers and databases to `{'`, `'.join(pronouns)}`.")

    @pronoun.command(name='lock')
    async def p_lock(self, ctx):
        """Prevents your pronouns from being changed in the current server.
        When trying to change pronouns in a locked server, it will only change your roles in that server.
        Used for if you're out to some server but not others."""
        added = await p_locks(ctx.author.id, ctx.guild.id)
        cmd = f"`{ctx.prefix}{self.p_lock.qualified_name}`"
        if added:
            await ctx.send(f"Your pronouns here will no longer be changed when syncing pronouns. Type {cmd} again to unlock.")
        else:
            await ctx.send(f"Your pronouns here will now be changed when syncing pronouns. Type {cmd} to lock.")
            await self.p_memberupdate(ctx.author.guild, [ctx.author], True)

    @pronoun.command(name='setup', hidden=True)
    async def p_setup(self, ctx):
        """Obsolete, use ;setup pronouns"""
        await ctx.send("_(command moved to ;setup pronouns)_")  # TODO: use i18n dumby

    @pronoun.command(name='update')
    @commands.cooldown(rate=1, per=12 * 60 * 60, type=commands.BucketType.guild)
    @commands.has_guild_permissions(manage_roles=True)
    @commands.bot_has_guild_permissions(manage_roles=True)
    async def p_update(self, ctx, force_sync: bool = False):
        """Checks database for current pronouns and *adds* roles accordingly.
        Should not really be used, bot should automagically update roles, so is severely rate limited as a result.
        Only use if roles are out of sync somehow (bot downtime perhaps?)
        Optionally add 'true' to remove any roles not in the database. This will ensure 100% accuracy with the database but will result in roles added by other sources being removed."""
        await self.p_memberupdate(ctx.guild, overwrite=force_sync)
        await ctx.send("Roles re-synchronized!")

    @pronoun.command(name='whitelist')
    async def p_whitelist(self, ctx):
        """Posts the current list of pronouns the bot will create."""
        whitelist = '`, `'.join(PRONOUN_WHITELIST)
        await ctx.send(
            "To prevent servers being flooded with roles, semicolon will only create missing pronoun roles if they are whitelisted. " +
            f"These are currently `{whitelist}`." +
            "\nPronouns outside of this list can still be added if servers contain that role, so try asking the mods to create a role for it.")

    @pronoun.command(name='list')
    async def p_list(self, ctx: commands.Context):
        """Posts the list of pronouns on the current server."""
        roles = [f"`{role.name.lower()[len(PRONOUN_PREFIX):]}`" for role in ctx.guild.roles if role.name.lower().startswith(PRONOUN_PREFIX)]
        await ctx.send(i18n['pronoun_list'].format(comma_separator(roles)))

def setup(bot):
    bot.add_cog(Pronouns())
