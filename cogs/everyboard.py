from typing import Union
import asyncio
from cogs.utils.utils import *
import logging


class Everyboard(commands.Cog):
    def __init__(self, bot: commands.Bot):
        self.bot = bot
        self.guild_id = 381941901462470658
        self.category_id = None
        self.thresh = 5
        self.lock = asyncio.Lock()
        self.json_lock = asyncio.Lock()
        self.json_cache = None
        self.json_filename = data_path('everyboard.json')
        if not os.path.exists(self.json_filename):
            with open(self.json_filename, 'w') as f:
                f.write('{}')
        with open(self.json_filename, 'r') as f:
            self.json_cache = json.load(f)
        if 'settings' not in self.json_cache:
            self.json_cache['settings'] = {}

        self.i18n = {  # TODO: make this a json file
            'category_name': 'everyboard',
            'misc_channel_name': 'everyotherboard',
            'misc_channel_topic': "The emojis that didn't make the cut",
            'unusable_emoji': '[?]',
            'message_content': '{0} **{1}** {2}',  # ie. :) **5** https://discordapp.com/channels/x/y/z
            'not_shown_name': 'Not Shown',
            'not_shown_value': '+{0} {1}{2}'  # ie. +2 attachments
        }

    async def save(self):
        async with self.json_lock:
            json_save(self.json_filename, self.json_cache)

    def format(self, emoji_in: discord.PartialEmoji, count: int, message: discord.Message):
        if emoji_in.is_unicode_emoji():
            emojistr = str(emoji_in)
        else:
            emojistr = self.i18n['unusable_emoji']
            getemoji = self.bot.get_emoji(emoji_in.id)
            if getemoji is not None:
                if getemoji.is_usable():
                    emojistr = str(emoji_in)
        content = self.i18n['message_content'].format(emojistr, count, message.jump_url)
        embed = discord.Embed(description=message.content, timestamp=message.created_at)
        embed.set_footer(text='#'+message.channel.name)
        embed.set_author(name=message.author.name, icon_url=message.author.avatar_url_as(format='png', size=128))
        # attachment code
        attachment_added = False
        misc = {'attachment': 0, 'embed': len(message.embeds)}
        for attachment in message.attachments:
            is_image_file = attachment.filename.split('.')[::-1][0].lower() in CONSTANTS['everyboard_extensions']
            if attachment_added or not is_image_file:
                misc['attachment'] += 1
                continue
            elif is_image_file:
                embed.set_image(url=attachment.url)
                attachment_added = True
        # display note if extra attachments/embeds couldn't be viewed
        embed_field = []
        for key, count in misc.items():
            if count > 0:
                embed_field.append(self.i18n['not_shown_value'].format(count, key, plural(count)))
        if embed_field:
            embed.add_field(name=self.i18n['not_shown_name'], value='\n'.join(embed_field))
        return {'content': content, 'embed': embed}

    async def process_reaction(self, payload: discord.RawReactionActionEvent):
        async with self.lock:
            if payload.guild_id != self.guild_id:
                return
            if str(payload.emoji) == '⭐':
                return
            sourcechan = self.bot.get_channel(payload.channel_id)
            if sourcechan.is_nsfw():
                return
            guild = self.bot.get_guild(payload.guild_id)
            message = await sourcechan.fetch_message(payload.message_id)
            mid = str(message.id)
            reactions = [x for x in message.reactions if str(x) == str(payload.emoji)]  # get the relevant reaction
            count = 0
            if reactions:  # if there are reactions
                reactions = reactions[0]  # get the first (only) element of the list
                async for user in reactions.users():
                    if user.id != message.author.id and not user.bot:  # count everyone who isn't OP or a bot
                        count += 1

            sid = str(message.guild.id)
            if sid not in self.json_cache:
                self.json_cache[sid] = {}
            guild_json = self.json_cache[sid]

            boardpost = None
            bpindex = str(payload.emoji) if payload.emoji.is_unicode_emoji() else str(payload.emoji.id)
            cacheindex = f"{mid}_{bpindex}"  # why nest dicts when you can just do this EZ
            if cacheindex in guild_json:  # if this is already in the database
                bpdata = guild_json[cacheindex]
                # get the message
                bpcid = bpdata['channel']
                bpmid = bpdata['message']
                bpc = self.bot.get_channel(bpcid)
                if bpc is not None:
                    boardpost = await bpc.fetch_message(bpmid)
                else:
                    logging.warning('boardpost channel is missing??')
                    # TODO: delete entry if channel is missing

                # if message doesn't pass threshold, ...
                if count < self.thresh:
                    await self.delete_message(boardpost.channel, boardpost, guild_json, cacheindex)
                    return
                action = boardpost.edit  # reusing minimal code *sounds* like a good idea but then it gives hacks l8r

            if count < self.thresh:
                return  # i feel i should write a comment for this but cmon. it just exits if under threshold

            if boardpost is None:  # if the board post wasn't found earlier, send one
                create_categories = False
                await self.get_category(create_categories)

                category = guild.get_channel(self.category_id)
                cname = payload.emoji.name if payload.emoji.name is not None else payload.emoji.id
                cname += 'board'
                # specify expected channel name, and a topic to prevent overlapping channels
                args = {'topic': str(payload.emoji.id) if payload.emoji.is_custom_emoji() else str(payload.emoji)}
                chan = discord.utils.get(category.text_channels, **args)
                args['name'] = cname
                if chan is None and not create_categories and len(category.channels) >= 49:
                    args = {'name': self.i18n['misc_channel_name'], 'topic': self.i18n['misc_channel_topic']}
                    chan = discord.utils.get(category.text_channels, **args)
                if chan is None:  # if no channel exists yet, make one
                    channel_limit = 50  # categories can have at max 50 channels. N/A if create_categories is true
                    if len(category.channels) == channel_limit:
                        return
                    chan = await category.create_text_channel(**args)
                action = chan.send
            boardpost2 = await action(**self.format(payload.emoji, count, message))
            if boardpost2 is not None:  # stupid hack because i don't rly know how to program.
                boardpost = boardpost2  # this hack triggers if action=chan.send
            guild_json[cacheindex] = {'channel': boardpost.channel.id,
                                      'message': boardpost.id,
                                      'source_chan': payload.channel_id}  # i probably don't need this last one...
            await self.save()

    @commands.Cog.listener()
    async def get_category(self, create_more: bool = False):
        # gets the category
        guild = self.bot.get_guild(self.guild_id)
        catname = self.i18n['category_name']
        category = None
        if create_more:
            categories = filter(lambda x: len(x.channels) < 50 and x.name == catname, guild.categories)
            if categories is not None:
                category = sorted(categories, key=lambda x: len(x.channels))[0]
        else:
            category = discord.utils.get(guild.categories, name=catname)
        if category is None:
            overwrites = {
                guild.default_role: discord.PermissionOverwrite(send_messages=False),
                guild.me: discord.PermissionOverwrite(manage_channels=True, send_messages=True)
            }
            category = await guild.create_category(name=catname, reason=catname.title()+'.', overwrites=overwrites)
            logging.info(f"Created {category.name}")
        self.category_id = category.id

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload: discord.RawReactionActionEvent):
        await self.process_reaction(payload)

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, payload: discord.RawReactionActionEvent):
        await self.process_reaction(payload)

    async def delete_message(self, channel_id: Union[int, discord.TextChannel], message_id: Union[int, discord.Message],
                             json_entry: dict, key: str):
        """Deletes a *board message and, if empty, the channel containing it"""
        msgchan = self.bot.get_channel(channel_id) if isinstance(channel_id, int) else channel_id  # msg-chan~
        msg = await msgchan.fetch_message(message_id) if isinstance(message_id, int) else message_id
        await msg.delete()
        del json_entry[key]
        msgs = await msgchan.history(limit=1).flatten()
        if len(msgs) == 0:  # if this was the only/last message in the channel,
            await msgchan.delete()  # delete it
        await self.save()  # save json

    async def reaction_clear(self, payload: Union[discord.RawReactionClearEvent, discord.RawReactionClearEmojiEvent]):
        async with self.lock:
            channel = self.bot.get_channel(payload.channel_id)
            if channel.guild.id != self.guild_id:
                return
            sid = str(channel.guild.id)
            if sid not in self.json_cache:
                return  # this server hasn't used everyboard
            guild_json = self.json_cache[sid]
            emojikey = ''
            if isinstance(payload, discord.RawReactionClearEmojiEvent):
                emojikey = str(payload.emoji) if payload.emoji.is_unicode_emoji() else str(payload.emoji.id)
            for key, data in guild_json.copy().items():
                startscheck = str(payload.message_id)+'_'+emojikey
                iskey = key.startswith(startscheck) if emojikey == '' else key == startscheck
                if iskey:
                    await self.delete_message(data['channel'], data['message'], guild_json, key)

        await self.save()  # save json

    @commands.Cog.listener()
    async def on_raw_reaction_clear(self, payload: discord.RawReactionClearEvent):
        await self.reaction_clear(payload)

    @commands.Cog.listener()
    async def on_raw_reaction_clear_emoji(self, payload: discord.RawReactionClearEmojiEvent):
        await self.reaction_clear(payload)

    @commands.Cog.listener()
    async def on_raw_message_delete(self, payload: discord.RawMessageDeleteEvent):
        if payload.guild_id != self.guild_id:  # block module on non-chip guilds
            return
        sid = str(payload.guild_id)
        if sid not in self.json_cache:
            return  # this server hasn't used everyboard
        guild_json = self.json_cache[sid]
        for key, data in guild_json.copy().items():
            if key.startswith(str(payload.message_id) + '_'):
                await self.delete_message(data['channel'], data['message'], guild_json, key)

    @commands.Cog.listener()
    async def on_guild_channel_delete(self, channel: discord.abc.GuildChannel):
        if not isinstance(channel, discord.TextChannel):
            return
        if channel.guild.id != self.guild_id:
            return
        sid = str(channel.guild.id)
        if sid not in self.json_cache:
            return  # this server hasn't used everyboard
        guild_json = self.json_cache[sid]
        for key, data in guild_json.copy().items():
            if data['channel'] == channel.id:
                del guild_json[key]
            # elif data['source_chan'] == channel.id:
            #     await self.delete_message(data['channel'], data['message'], key)
        await self.save()

    @commands.group(name='everyboard', invoke_without_command=True)
    @commands.is_owner()
    async def everyboard_cmd(self, ctx: commands.Context):
        # TODO: call help cmd function? idk
        await ctx.send("Use one of the sub-commands dummy")

    @everyboard_cmd.command(name='reset')
    @commands.is_owner()
    async def cmd_reset(self, ctx: commands.Context):
        """Clears all everyboard channels in the current guild"""
        for cat in filter(lambda x: x.name == self.i18n['category_name'], ctx.guild.categories):
            for chan in cat.channels:
                await chan.delete()
        del self.json_cache[str(ctx.guild.id)]
        await self.save()
        await ctx.send("categories cleared")

    @everyboard_cmd.command(name='sync')
    @commands.is_owner()
    async def cmd_sync(self, ctx: commands.Context):
        """Unfinished, would've synced everyboard channels"""
        async with self.lock:
            for key, data in self.json_cache.copy().items():
                splimt = key.split('_')
                source_msg = key[0]
                emoj = '_'.join(key[1:])
                # got bored of writing this right about here. mostly useless cmd anyway

    @everyboard_cmd.command(name='stats')
    @commands.is_owner()
    async def cmd_stats(self, ctx: commands.Context, channel: discord.TextChannel):
        async with ctx.typing():
            csv = []
            msgs = 0
            async for msg in channel.history(limit=None, oldest_first=True):
                if msg.author.id != self.bot.user.id:
                    continue
                msgs += 1
                csv.append('{0}, {1}'.format(str(msg.created_at), msgs))
            outfile = f'{channel.name} {channel.id}.csv'
            with open(outfile, "w") as f:
                f.write('\n'.join(csv))
            await ctx.send(f'Data saved to {outfile}')


def setup(bot):
    bot.add_cog(Everyboard(bot))
