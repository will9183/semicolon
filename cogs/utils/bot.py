import datetime

import aiohttp
import discord
import asyncio
from discord.ext import commands
import logging

from .utils import *
from .lang import *
import numpy as np


class GuildDisabledCommand(commands.CheckFailure):
    pass


class Semicolon(commands.Bot):
    def __init__(self, *args, **kwargs):
        self.session = aiohttp.ClientSession()

        os.makedirs(CONSTANTS['data_folder'], exist_ok=True)
        logging.basicConfig(level=logging.WARN, format='[%(name)s %(levelname)s] %(message)s')
        self.logger = logging.getLogger('bot')

        super().__init__(
            command_prefix=discord.ext.commands.when_mentioned_or(CONSTANTS['prefix_str']),
            description=i18n['description'],
            case_insensitive=True,
            activity=discord.Game(f"{CONSTANTS['prefix_str']}help for help!"),
            allowed_mentions=discord.AllowedMentions(everyone=False, users=False, roles=False),
            *args,
            **kwargs
        )

        self.add_check(self.blocked_guild)

    async def on_ready(self):
        # overly complicated fancy formatting
        def get_str_length(number):
            number = len(str(number))
            return np.math.ceil(number / 3) - 1 + number

        big_numbers = tuple(map(len, [self.guilds, list(self.get_all_channels()), self.users]))
        guilds, channels, users = big_numbers
        pad_to = max(map(get_str_length, big_numbers))
        pad_str = "{:>" + str(pad_to) + ",}"
        print("Logged in as {0} ({0.id})".format(self.user))
        print("Guilds:   " + pad_str.format(guilds))
        print("Channels: " + pad_str.format(channels))
        print("Users:    " + pad_str.format(users))

    async def on_command_error(self, ctx: commands.Context, error: Exception):
        passable_errors = commands.ConversionError, commands.UserInputError, commands.CheckFailure, \
                          commands.DisabledCommand, commands.CommandOnCooldown, commands.MaxConcurrencyReached, \
                          GuildDisabledCommand
        delete = 10

        if isinstance(error, commands.CommandNotFound) or \
                (any([isinstance(error, x) for x in passable_errors]) and ctx.command.name == '-;'):
            pass
        elif isinstance(error, commands.CommandOnCooldown) and ctx.command.name == "catch":
            embed = embed_author_template(ctx, subtle_author=True)
            embed.description = i18n['pokemon_ratelimit']
            embed.colour = discord.Colour.greyple()
            await ctx.send(embed=embed)
        elif isinstance(error, commands.CommandOnCooldown):
            stopwait = ctx.message.created_at + datetime.timedelta(seconds=error.retry_after)
            chn = ctx.channel
            botself = ctx.guild.me if isinstance(chn, discord.TextChannel) else chn.me
            perms = chn.permissions_for(botself)
            if perms.add_reactions and perms.read_message_history:
                react_index = CONSTANTS['emojis']['RATELIMIT'][int(not perms.use_external_emojis)]
                await ctx.message.add_reaction(react_index)
                thereact = None
                while not thereact:
                    thereact = list(filter(lambda x: str(x) == react_index, ctx.message.reactions))
                    if not thereact:  # avoid sleeping if unnecessary
                        await asyncio.sleep(1)
                await discord.utils.sleep_until(stopwait)
                try:
                    await thereact[0].remove(botself)
                except discord.errors.NotFound:
                    pass
        elif isinstance(error, GuildDisabledCommand):
            await ctx.send(f"{ctx.guild.name} has disabled that command.", delete_after=delete)
        elif isinstance(error, commands.NotOwner):
            # await ctx.send("You cannot access that command.")
            pass
        elif isinstance(error, commands.CommandInvokeError):
            cmention = f"in {ctx.channel.mention}" if isinstance(ctx.channel, discord.TextChannel) else 'in DMs'
            ename = str(type(error).__name__)
            emsg = i18n['error_dm'].format(ename, cmention, ctx.author.mention, ctx.message.content, error)
            await (await self.application_info()).owner.send(emsg)
        else:
            await ctx.send(embed=error_gen(ctx, str(error)))

    async def blocked_guild(self, ctx):
        """
        Checks if the guild has blocked this command
        """
        if not ctx.guild:
            return True
        if ctx.author.guild_permissions.administrator:
            return True

        # TODO: should find a way to cache this, i don't imagine this is super efficient?
        filepath = data_path('disabled_commands.json')
        if os.path.exists(filepath):
            with open(filepath) as j:
                data = json.load(j)
            json_key = str(ctx.guild.id)
            if json_key in data:  # if guild is in data
                cmd = ctx.command
                while cmd.parent is not None:  # get the base command
                    cmd = cmd.parent

                if cmd.name in data[json_key]:  # if cmd name is in disabled list
                    raise GuildDisabledCommand
        return True

    def run(self):
        token = open('token.txt', 'r').read().split('\n')[0].strip()

        cog_path = 'cogs'
        for cog in ['.'.join([cog_path, f.split('.')[0]]) for f in os.listdir(cog_path) if f.endswith('.py')]:
            try:
                self.load_extension(cog)
            except Exception as e:
                self.logger.exception('Failed to load cog {}.'.format(cog))
            else:
                self.logger.info('Loaded cog {}.'.format(cog))

        super().run(token)
