CONSTANTS = {  # meta bot data that generally shouldn't be translated or changed unless hosting your own bot
    'prefix_str': ';',
    'join_channel': 'semicolon_joins',
    'message_channel': 'semicolon_messages',
    'data_folder': 'data',
    'chip': 741770822896844891, #not chip but bot testing server because it is
    'max_pokemon': 807,
    'pkmn_sprites_dir': 'pkmn_sprites',
    'emojis': {  # from Bot-Moji: https://discord.gg/SyKBWjP
        "JOIN": ['<:enter:741780026114179072>', '\N{BLACK RIGHTWARDS ARROW}'],
        "LEAVE": ['<:quit:741780026223231098>', '\N{AIRPLANE DEPARTURE}'],
        "BAN": ['<:banned:741780025891881063>', '\N{HAMMER}'],
        "UNBAN": ['<:unbanned:741780025740886148>', '\N{OPEN LOCK}'],
        "RATELIMIT": ['<:refresh:741780025975767060>', '\N{CLOCK FACE THREE OCLOCK}'],
        "NEW_JOIN": "\N{CLOCK FACE THREE OCLOCK} ",
        "DM_SENT": '\N{OPEN MAILBOX WITH RAISED FLAG}'
    },
    'colors': {
        'default': 0x88297c,
        'message_edit': 0xf5ba5b,
        'message_delete': 0xde3d28
    },
    'everyboard_extensions': ['jpg', 'png', 'gif', 'jpeg'],  # embeddable file types
    'pronoun_whitelist': ['she/her', 'he/him', 'they/them', 'any pronouns', 'no pronouns'],  # default/global roles
    'pronoun_prefix': '[p] ',
    'reddit_sort': ['hot', 'new', 'rising', 'top', 'controversial'],
    'reddit_timeable': ['top', 'controversial'],
    'reddit_time': ['hour', 'day', 'week', 'month', 'year', 'all'],
    'self_delete': 10  # how long to wait before deleting approval messages
}

i18n = {
    # utils/bot.py: meta text, errors, etc
    'description': '''Collection of various tools for entertainment, moderation, and more.
                      Created by lexikiq#0493
                      Hosted by will9183#9097''',
    'error': 'ERROR: ',
    'error_dm': "{0} {1} by {2} running `{3}`:```{4}```",
    'guild_exclusive': "This command is only enabled on {}.",
    'invalid_argument': '`{}` must be one of `{}`.',
    'connection_failure': "Connection to {} failed. (`{} {}`)",
    'secret_guild': "[DATA EXPUNGED]",
    'too_long': "... [output too long]",
    'imported': 'Import complete!',
    # leaderboard.py
    'points_description': 'You earned {0.score} points.',  # You earned 5 points.
    'guess_description_True': "That's right, I was thinking of {0.score}!",
    'float_guess_description_True': "W-what? That's right! I was thinking of {0.score}!",
    'guess_description_False': 'Sorry {1.mention}, you guessed {0.user} but I was thinking of {0.score}!',
    'float_guess_description_False': 'Sorry {1.mention}, you guessed {0.user} but I was thinking of {0.score}!',
    'guess_error_max_guess_too_low': 'Your maximum guess range should be greater than 1!',
    'guess_error_guess_too_low': "Your guess can't be lower than 1!",
    'guess_error_guess_too_large': 'Your guess ({0}) was greater than the maximum range ({1})!',
    'guess_error_number_too_long': "Your numbers should be less than {2} characters long! You're not that lucky...",
    'float_guess_error_number_too_long': "Your float should have {1} decimal points at most!",
    'float_guess_error_range': 'Your guess should be between [0, 1)!',
    'no_leaderboard': 'No leaderboards are available!',
    'best_name': 'New {} best!',  # New personal best!
    'low_name': 'New {} low!',    # New personal low!
    'points_value': 'You beat {0} {1} {2} of {3} point{4}!',  # You beat your personal best of 5 points!
    'guess_value': 'You beat {0} {1} best range of {3}!',     # You beat your personal best range of 5!
    'float_guess_value': '[how did you get this message]',
    'user_data': 'personal',  # used in best_name, low_name, points_value {1}, guess_value {1}
    'global_data': 'global',  # used in best_name, low_name, points_value {1}, guess_value {1}
    'user_data_value': 'your',   # used in points_value {0}, guess_value {0}
    'global_data_value': 'the',  # used in points_value {0}, guess_value {0}
    'top_msg': '`{0[count]:>2}.` {0[member]!s} - ',  # 1. @lexikiq#0493:
    'points_top_suffix': '{0[score]:.5f} points',  # 3.14159 points
    'generic_top_suffix': '{0[score]}',  # 3
    'best_top_name': 'Leaderboard:',
    'low_top_name': 'Lowderboard:',
    # pokemon.py
    'pokemon_name': '{0.name} (#{0.dex})',
    'pokemon_no_sprite': '(no sprite is available for this Pokémon)',
    'pokemon_catch': 'You caught {}!',
    'pokemon_new': 'It has been added to your Pokédex!',
    'pokemon_old': "It's already in your Pokédex!",
    'pokemon_ratelimit': "There don't seem to be any Pokémon here yet...",
    # misc.py
    'count_cmd': 'Found {0} instance{4} of `{2}` from {1!s} across {3} public channel{5}.',  # 4/5 are 's' if plural
    # joinlog.py
    'joinlog_prefix': '{0} {1.mention} ({1!s}) ',
    'joinlog_JOIN': "joined the server. (User #{2})\n{3}Account created {1.created_at:%b %d %Y %H:%M:%S}",
    'joinlog_LEAVE': "left the server. (New user count: {2})",
    'joinlog_BAN': "was banned from the server. {}",
    'joinlog_reason': 'Reason: {4}',
    'joinlog_UNBAN': "was unbanned from the server.",
    'joinlog_NAME': '{1.mention} changed their name from {0!s} to {1!s}',
    'joinlog_DISCRIM': "_User's discriminator changed!_",
    'joinlog_ONLY_DISCRIM': "{1.mention} ({1!s})'s discriminator changed from #{0.discriminator:0>4} to #{1.discriminator:0>4}",
    'message_edit_title': "Message edited in #{}",  # {} is a channel name
    'message_edit_desc': "[Jump to Message]({})",
    'message_edit_new_title': "New Content",  # title for new content of an edited message
    'message_edit_old_title': "Old Content",  # title for original content of an edited message
    'message_delete_title': "Message deleted in #{}",  # {} is a channel name
    'message_delete_uncached_title': "Uncached message deleted in #{}",  # {} is a channel name
    'message_delete_footer': "Message ID: {}",
    'message_blank_content': "{**semicolon:** `content` is blank!}",
    # lounge.py
    'lcl_not_found': "The next sequence couldn't be found!",
    'lcl_channel_missing': "That channel isn't setup!",
    # moderation.py
    'setup_default': "Please specify a feature to setup. To get the list of features, use `{}help {}`.",
    'channel_created': "{0.mention} has been successfully created! Please set appropriate permissions, such as hiding the channel from the public if desired, and avoid changing the channel name.",
    'channel_fail': "{0.mention} already exists!",
    'reason': "Requested by {}",
    'join_channel_topic': 'Logs new and leaving users, bans, unbans, username changes, and discriminator changes.',
    'message_channel_topic': "Logs edited and deleted messages.\n(Assuming they've been sent recently to appear in the cache.)",
    'pronouns_created': "Pronoun roles successfully created!",
    # pronouns.py
    'pronoun_list': "This server's list of pronouns are {}",
    "pronoun_dm_fail": "{}: `{}` has been {}.\n_If you allow DMs, you can get the list of successful servers messaged to you._"
}
