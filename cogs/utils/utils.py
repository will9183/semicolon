import os
import discord
from discord.ext import commands
import typing
import json
from cogs.utils.lang import *


class JsonManager:
    def __init__(self, filename):
        self._data = {}
        self._file = os.path.join(CONSTANTS['data_folder'], filename)
        os.makedirs(CONSTANTS['data_folder'], exist_ok=True)
        if not os.path.exists(self._file):
            self._save()
        self._load()

    def _load(self):
        self._data = json_load(self._file)

    def _save(self):
        json_save(self._file, self._data)

    def init_data(self, key: str, init_with=None):
        """Creates objects in json dict if non-existent"""
        if init_with is None:
            init_with = {}
        if key not in self._data:
            self._data[key] = init_with


def json_load(filename: str):
    """Loads JSON file"""
    with open(filename, 'r') as f:
        return json.load(f)


def json_save(filename: str, cache: dict):
    """Saves a dict to minified json file"""
    with open(filename, 'w') as f:
        json.dump(cache, f, separators=(',', ':'))


class GuildExclusiveCommand(commands.CheckFailure):
    pass


def guild_check(ctx: commands.Context, guild_id: int, no_name: bool = False) -> bool:
    """
    Checks if the context matches an input guild ID, and raises a descriptive error if not.
    :param ctx: context from a command
    :param guild_id: guild id to check against
    :param no_name: (optional) hides the expected server name if True
    :return: True or raises a GuildExclusiveCommand error
    """
    if ctx.guild and ctx.guild.id == guild_id:
        return True
    if no_name:
        guild = i18n['secret_guild']
    else:
        guild = ctx.bot.get_guild(guild_id)
        guild = guild_id if guild is None else guild
    raise GuildExclusiveCommand(i18n['guild_exclusive'].format(guild))


async def is_chip(ctx: commands.Context) -> bool:
    """Returns bool of if current guild is ChiP or not. Hides the name because it's funny."""
    return guild_check(ctx, 381941901462470658, no_name=True)


def embed_author_template(ctx: commands.Context, subtle_author: typing.Union[bool, None] = False) -> discord.Embed:
    """Creates an embed template"""
    embed = discord.Embed(timestamp=ctx.message.created_at, color=CONSTANTS['colors']['default'])
    if subtle_author is not None:
        if not subtle_author:
            if isinstance(ctx.channel, discord.TextChannel):
                embed.set_footer(text='#' + ctx.channel.name)
            embed.set_author(name=ctx.author.name, icon_url=ctx.author.avatar_url_as(format='png', size=128))
        else:
            embed.set_footer(text='Requested by '+str(ctx.author))
    return embed


def ping_user(ctx: typing.Union[commands.Context, discord.Message, discord.abc.Snowflake]) -> discord.AllowedMentions:
    """Returns an AllowedMentions instance of the author or snowflake-type"""
    # TODO: process raw IDs somehow ?
    # TODO: process multiple inputs
    if isinstance(ctx, discord.ext.commands.Context) or isinstance(ctx, discord.Message):
        user = ctx.author
    elif isinstance(ctx, discord.abc.Snowflake):
        user = ctx
    else:
        raise ValueError("Unknown input type")
    return discord.AllowedMentions(everyone=False, users=[user], roles=False)


async def private_message(ctx: commands.Context, msg_to_send: str):
    """Privately messages command output"""
    await ctx.author.send(msg_to_send)
    perms = ctx.channel.permissions_for(ctx.guild.me)
    emoji = CONSTANTS['emojis']['DM_SENT']
    if perms.add_reactions and perms.read_message_history:
        await ctx.message.add_reaction(emoji)
    else:
        await ctx.send("Output privately messaged "+emoji)


def comma_separator(lst: list, oxford_comma: bool = True) -> str:
    """Creates a comma separated list, ie 'Joe, Bob, and Frank'"""
    length = len(lst)
    if length > 1:
        output_str = ', '.join(lst[:-1])
        comma = ',' if length > 2 and oxford_comma else ''
        output_str = output_str + comma + ' and ' + lst[length - 1]
    else:
        output_str = ''.join(lst)
    return output_str


def clamp(n: int, minimum: int, maximum: int) -> int:
    """Clamps an input n to fit in range [minimum, maximum]"""
    return max(minimum, min(n, maximum))


def plural(number_of_items: int) -> str:
    """Returns an 's' if number_of_items isn't 1"""
    return 's' if number_of_items != 1 else ''


def unpack_args(ctx) -> tuple:
    return ctx.args[2:]  # gets direct command inputs, removing the 'self' and 'ctx'


def error_gen(ctx: commands.Context, error_message: str) -> discord.Embed:
    """Returns an embed containing the input error message and a footer noting the command requester."""
    embed = embed_author_template(ctx, subtle_author=True)
    embed.description = error_message
    embed.colour = discord.Colour.red()
    return embed


def data_path(filename: str):
    return os.path.join(CONSTANTS['data_folder'], filename)


def shorten(string: str, limit: int = 2000):
    if len(string) <= limit:
        return string
    ending = i18n['too_long']
    return string[:limit-len(ending)] + ending
