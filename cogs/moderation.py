import asyncio
import json
import os
import typing

import discord
from discord.ext import commands
import datetime
from cogs.utils.utils import *


async def channel_setup(ctx: commands.Context, constants_key: str):
    cname = CONSTANTS[constants_key]
    existing_channel = discord.utils.get(ctx.guild.text_channels, name=cname)
    if existing_channel:
        await ctx.send(i18n['channel_fail'].format(existing_channel))
        return
    create_in = ctx.guild.get_channel(ctx.channel.category_id) if ctx.channel.category_id else ctx.guild
    overwrites = {
        ctx.guild.default_role: discord.PermissionOverwrite(send_messages=False),
        ctx.guild.me: discord.PermissionOverwrite(send_messages=True, read_messages=True)
    }
    new_chan = await create_in.create_text_channel(CONSTANTS[constants_key], topic=i18n[constants_key+'_topic'],
                                                   overwrites=overwrites,
                                                   reason=i18n['reason'].format(ctx.author))
    await ctx.send(i18n['channel_created'].format(new_chan))


class Moderation(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.disabled_cmds = data_path('disabled_commands.json')
        if not os.path.exists(self.disabled_cmds):
            with open(self.disabled_cmds, 'w') as f:
                f.write('{}')

    @commands.group(invoke_without_command=True)
    @commands.guild_only()
    async def setup(self, ctx: commands.Context):
        """Sets up a semicolon feature on a server."""
        await ctx.send(i18n['setup_default'].format(ctx.prefix, ctx.invoked_with))

    @setup.command(name='joinlog', brief='Creates a channel that logs joins, leaves, & name changes.')
    @commands.has_guild_permissions(manage_channels=True)
    @commands.bot_has_guild_permissions(manage_channels=True)
    @commands.guild_only()
    async def setup_joinlog(self, ctx: commands.Context):
        """Creates a channel that logs member joins, leaves, bans, unbans, name changes, and discriminator changes."""
        await channel_setup(ctx, 'join_channel')

    @setup.command(name='messagelog')
    @commands.has_guild_permissions(manage_channels=True)
    @commands.bot_has_guild_permissions(manage_channels=True)
    @commands.guild_only()
    async def setup_messages(self, ctx: commands.Context):
        """Creates a channel that logs edited and deleted messages."""
        await channel_setup(ctx, 'message_channel')

    @setup.command(name='pronouns')
    @commands.has_guild_permissions(manage_roles=True)
    @commands.bot_has_guild_permissions(manage_roles=True)
    async def setup_pronouns(self, ctx: commands.Context):
        """Sets up automatic, cross-server pronoun roles."""
        toprole = ctx.guild.me.top_role
        for pronoun in CONSTANTS['pronoun_whitelist']:
            p_name = CONSTANTS['pronoun_prefix'] + pronoun
            p_role = discord.utils.find(lambda r: r.name.lower() == p_name and toprole > r, ctx.guild.roles)
            if not p_role:
                await ctx.guild.create_role(name=CONSTANTS['pronoun_prefix'] + pronoun)
        await ctx.send(i18n['pronouns_created'])

    @commands.command()
    @commands.has_permissions(manage_guild=True)
    @commands.guild_only()
    async def toggle(self, ctx: commands.Context, command: str, action: bool):
        """
        Enables or disables a command for the current guild.
        """
        cmd = ctx.bot.get_command(command)

        if cmd is None:
            await ctx.send("Command not found!")
            return

        while cmd.parent is not None:  # get the base command
            cmd = cmd.parent

        if cmd.name in ['toggle']:
            await ctx.send("You cannot disable this command!")
            return

        with open(self.disabled_cmds) as j:
            data = json.load(j)
        json_key = str(ctx.guild.id)  # json can't save ints as keys
        if json_key not in data:
            data[json_key] = []
        gdata = data[json_key]

        # i tried to lessen duplicate code but im not sure if this is actually good or not fhgdsfdsf
        success_str = "Successfully {}d `" + ctx.prefix + cmd.name + "` in " + ctx.guild.name + "."
        failure_str = "`" + ctx.prefix + cmd.name + "` is already {}d in " + ctx.guild.name + "."

        if not action:
            status = 'disable'
            format_with = failure_str

            if cmd.name not in gdata:
                gdata.append(cmd.name)
                format_with = success_str

        else:
            status = 'enable'
            format_with = failure_str

            while cmd.name in gdata:  # just incase it duped?? idk
                gdata.remove(cmd.name)
                format_with = success_str
            if len(gdata) == 0:
                del data[json_key]

        with open(self.disabled_cmds, 'w') as j:
            json.dump(data, j)

        await ctx.send(format_with.format(status))

    @commands.group(aliases=['purge', 'delete'], invoke_without_command=True)
    @commands.has_permissions(manage_messages=True)
    @commands.bot_has_permissions(manage_messages=True)
    async def clear(self, ctx, messages: int):
        """
        Deletes X amount of messages.
        X does not include the message invoking the command.
        Max of 1000 messages.
        """
        messages = min(messages+1, 1001)  # add the cmd-invoking msg, limit to 1000 messages deleted
        await ctx.channel.purge(limit=messages, bulk=True)

    @clear.command(name='after')
    @commands.has_permissions(manage_messages=True)
    @commands.bot_has_permissions(manage_messages=True)
    async def clear_after(self, ctx, message_id: int):
        """
        Clears messages sent after a provided message ID.
        Command works by extracting the time from the ID and deleting anything afterwards, so any ID is valid input.
        Will ask for confirmation if you try to clear over 1 days worth of messages.
        """
        dtstop = discord.utils.snowflake_time(message_id)  # gets datetime object corresponding to the input

        async def the_purge():
            await ctx.channel.purge(after=dtstop, limit=10000)

        if (datetime.datetime.now() - dtstop) > datetime.timedelta(days=1):  # clearing over a day of messages
            timeout = 60.0  # how long to wait for input
            perms = ctx.channel.permissions_for(ctx.guild.me)
            author_perms = ctx.author.permissions_in(ctx.channel)
            dest = ctx if perms.send_messages else ctx.author  # send message to DMs if no chatting perms
            # TODO: there's probably a better way to write the ending dest == ctx.author
            can_react = (perms.add_reactions and perms.read_message_history and author_perms.read_message_history) or dest == ctx.author
            can_custom = perms.external_emojis or dest == ctx.author
            emojis = {True: [ctx.bot.get_emoji(328630479886614529), ctx.bot.get_emoji(328630479576104963)], False: ['✅', '❌']}
            use_emojis = emojis[can_custom]

            output = "This will purge over a day's worth of messages, are you sure?"
            output = output+" (type `y` or `n`)" if not can_react else output
            msg = await dest.send(output)
            if can_react:
                for r in use_emojis:
                    await msg.add_reaction(r)

            def react_check(reaction, user):
                return user == ctx.message.author and str(reaction.emoji) in list(map(str, use_emojis)) and reaction.message.id == msg.id

            def msg_check(m):
                return m.author == ctx.message.author and m.content.lower() in ['y', 'n'] and m.channel == ctx.channel

            try:
                if can_react:
                    userreaction, useruser = await ctx.bot.wait_for('reaction_add', timeout=timeout, check=react_check)
                    confirmation = str(userreaction) == str(use_emojis[0])
                else:
                    usermsg = await ctx.bot.wait_for('message', timeout=timeout, check=msg_check)
                    confirmation = usermsg.content.lower() == 'y'
                    if dest == ctx and not confirmation:
                        await usermsg.delete()
                if confirmation:
                    await the_purge()
                else:
                    await ctx.message.delete()
                    await msg.delete()
            except asyncio.TimeoutError:
                await msg.delete()
                await ctx.message.delete()
        else:
            await the_purge()


def setup(bot):
    bot.add_cog(Moderation(bot))
